<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\Project;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    // Listing all projects
    public function projectListings(): Factory|View|Application
    {
        return view('components.projectlistings', [
            'projects' => Project::orderBy('created_at', 'desc')->paginate(3)
        ]);
    }

    // Create New Project
    public function createProject(Request $request)
    {
        $request->validate([
            'project_name' => 'required|min:3',
        ]);

        Project::create([
            'project_name' => request('project_name'),
            'user_id' => auth()->id()
        ]);

        return redirect('/create-board')->with('message', 'Project Created!');
    }

    // show settings
    public function showSetting(): Factory|View|Application
    {
        return view('components.settings');
    }


}
