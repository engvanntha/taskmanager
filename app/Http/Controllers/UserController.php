<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    // Show Registration Page
    public function register(){
        return view('users.register');
    }
    // Do Registration
    public function doRegister(Request $request){
        $formFields = $request->validate([
            'name' => ['required', 'min:3'],
            'email' => ['required', 'email', Rule::unique('users', 'email')],
            'password' => 'required|min:6',
            'passwordConfirmation' => 'required|min:6|same:password'
        ]);

        // Hash password
        $formFields['password'] = Hash::make($formFields['password']);
        // Create user
        $user = User::create($formFields);
        $user->setRememberToken(Str::random('100'));
        $user->save();
        //Login
        auth()->login($user);
        // Return to homepage
        return redirect('/')->with('message', 'User created successfully');
    }

    // Login User Out
    public function logout(Request $request){
        auth()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/login')->with('message', 'You`ve logged out');
    }


    // Show Login Page
    public function login(){
        return view('users.login');
    }
    // Do Login
    public function doLogin(Request $request){
        $formFields = $request->validate([
            'email' => ['required', 'email'],
            'password' => 'required'
        ]);

        if (auth()->attempt($formFields)){
            $request->session()->regenerateToken();
            return redirect('/')->with('message', 'You`ve been logged in!');
        }

        return back()->withErrors(['email'=>'Invalid credentials']);
    }

}
