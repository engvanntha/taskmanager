@extends('index')
@section('content')
    {{--    Add board to list--}}
    <div class="flex justify-center items-center font-bold">

        <div class="bg-white mb-6 sm:basis-3/4 basis-full sm:mr-8 sm:pb-0">
            <form action="/create-board" method="post">
                @csrf
                <div class="relative border-2 border-gray-100 m-4 rounded-lg">
                    <div class="absolute top-4 left-3">
                        <i
                            class="fa fa-plus text-gray-400 z-20 hover:text-gray-900"
                        ></i>
                    </div>

                    <input
                        type="text"
                        name="board_name"
                        class="h-14 w-full pl-10 pr-20 rounded-lg z-0 focus:shadow focus:outline-none"
                        placeholder="Add new board..."
                    />
                    @error('board_name')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror


                    <div class="absolute top-2 right-2">
                        <button
                            type="submit"
                            class="h-10 w-20 text-white rounded-lg bg-red-500 hover:bg-red-600"
                        >
                            Add
                        </button>
                    </div>

                </div>
            </form>
        </div>
    </div>

    {{--    Board if exists here    --}}
    @unless(count($boards) == 0)
        <div class="gap-4">
            @foreach($boards as $board)
                <div class="flex justify-center inline-grid flex-wrap">
                    <div class="relative border-2 border-gray-100 m-4 bg-appColor rounded-lg h-fit w-96">
                        <div class="flex justify-center rounded-lg font-bold text-white p-1 bg-appColor">
                            {{ $board->board_name }}
                            <div
                                class="flex justify-end place-items-end rounded-lg font-bold text-white p-1 bg-appColor">
                                <i class=" fa-solid fa-pen-to-square"></i>
                            </div>
                        </div>

                        <form action="/add-card" method="post">
                            @csrf
                            <div class="flex items-center">
                                <i
                                    class="absolute fa fa-plus ml-2.5 text-gray-400 z-20 hover:text-gray-900"
                                ></i>
                                <input
                                    type="text"
                                    name="card_item"
                                    class=" h-10 w-56 pl-10 pr-20 rounded-lg z-0 focus:shadow focus:outline-none"
                                    placeholder="Add new card..."
                                />
                                @error('card_item')
                                <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                                @enderror

                                <button
                                    type="submit"
                                    class="h-10 w-20 text-white rounded-lg bg-red-400 hover:bg-red-600"
                                >
                                    <i class="fa-solid fa-plus"></i>
                                </button>
                            </div>
                        </form>
                        <form method="post" action="/boards/{{$board->id}}">
                            @csrf
                            @method('delete')
                            <button
                                type="submit"
                                class="h-10 w-20 text-white rounded-lg bg-red-500 hover:bg-red-600"
                            >
                                <i class="fa-solid fa-trash"></i>
                            </button>
                        </form>

                        @unless(count($cards) == 0)
                            @foreach($cards as $card)
                                <div class="border-2 border-gray-50 rounded-lg text-white m-2 p-2 hover:bg-amber-700">
                                    {{ $card->card_item }}
                                    <div class="flex justify-end place-items-center">
                                        <i class="fa-solid fa-pen-to-square"></i>
                                        <i class="fa-solid fa-trash"></i>
                                    </div>

                                </div>
                            @endforeach
                        @else
                            <div class="text-xl text-white grid h-fit place-items-center">
                                <div class="mb-6 sm:basis-3/4 basis-full sm:mr-8 sm:pb-0">
                                    No card created yet!
                                </div>
                            </div>
                        @endunless

                    </div>
                </div>
            @endforeach

        </div>

        {{--   No board here   --}}
    @else
        <div class="text-4xl grid h-fit place-items-center">
            <div class="bg-white mb-6 sm:basis-3/4 basis-full sm:mr-8 sm:pb-0">
                No board created yet!
            </div>
        </div>
    @endunless
@endsection
