@extends('index')
@section('content')
    <div class="flex">
        <div class="bg-gray-50 border border-gray-200 p-10 rounded max-w-lg mx-auto mt-2">
            <header class="text-center">
                <h2 class="text-xl font-bold uppercase mb-1">
                    Edit info
                </h2>
            </header>
            <form action="/edit-info/{{auth()->user()->id}}" method="post">
                @csrf
                @method('put')
                <div class="mb-6">
                    <label for="name" class="inline-block text-lg mb-2">
                        Name
                    </label>
                    <input
                        type="text"
                        class="border border-gray-200 rounded p-2 w-full"
                        name="name"
                        value="{{ old(auth()->user()->name) }}"
                    />
                    @error('name')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>


                <div class="mb-6">
                    <label for="password" class="inline-block text-lg mb-2"
                    >Old Password</label
                    >
                    <input
                        type="password"
                        class="border border-gray-200 rounded p-2 w-full"
                        name="oldPassword"
                    />
                    @error('email')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mb-6">
                    <label
                        for="password"
                        class="inline-block text-lg mb-2"
                    >
                        New Password
                    </label>
                    <input
                        type="password"
                        class="border border-gray-200 rounded p-2 w-full"
                        name="password"
                        value="{{ old('password') }}"
                    />
                    @error('password')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mb-6">
                    <label
                        for="passwordConfirmation"
                        class="inline-block text-lg mb-2"
                    >
                        Confirm Password
                    </label>
                    <input
                        type="password"
                        class="border border-gray-200 rounded p-2 w-full"
                        name="passwordConfirmation"
                        value="{{ old('passwordConfirmation') }}"
                    />
                    @error('passwordConfirmation')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>

                <div class="flex justify-between items-center">
                    <button
                        type="submit"
                        class="bg-appColor mr-3.5 text-white rounded py-2 px-4 hover:bg-black"
                    >
                        Save
                    </button>
                    <button
                        type="submit"
                        class=" bg-gray-600 mr-3.5 text-white rounded py-2 px-4 hover:bg-black"
                    >
                        Back
                    </button>
                </div>



            </form>
        </div>
    </div>


@endsection
