@extends('index')
@section('content')
    <div class="w-4/5 mx-auto pb-10">
        {{--    Create New Project     --}}
        <form action="/create-project" method="post">
            @csrf
            <div class="relative border-2 border-gray-100 m-4 rounded-lg">
                <div class="absolute top-4 left-3">
                    <i
                        class="fa fa-plus text-gray-400 z-20 hover:text-gray-900"
                    ></i>
                </div>

                <input
                    type="text"
                    name="project_name"
                    class="h-14 w-full pl-10 pr-20 rounded-lg z-0 focus:shadow focus:outline-none"
                    placeholder="Add new project..."
                />
                @error('project_name')
                <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                @enderror
                <div class="absolute top-2 right-2">
                    <button
                        type="submit"
                        class="h-10 w-20 text-white rounded-lg bg-red-500 hover:bg-red-600"
                    >
                        Create
                    </button>
                </div>
            </div>
        </form>

        @foreach($projects as $project)
            <div
                class="bg-white mb-6 pt-10 rounded-3xl drop-shadow-lg sm:basis-3/4 basis-full sm:mr-8 pb-10 sm:pb-0 hover:bg-appColor">
                <div class="w-11/12 mx-auto pb-10">
                    <h2 class="text-gray-900 text-2xl font-bold pt-6 pb-0 sm:pt-0 hover:text-gray-700 transition-all">
                        <a href="/create-board/{{$project->id}}">
                            {{$project->project_name}}
                        </a>
                    </h2>

                    <span class="text-gray-500 text-sm sm:text-base">
                    Owned by:
                        <a href=""
                           class="text-green-500 italic hover:text-green-400 hover:border-b-2 border-green-400 pb-3 transition-all">
                            Eng Vanntha
                        </a>
                    last updated : {{$project->updated_at}}
                    </span>
                    <div class="flex">
                        <form class="mr-3.5" action="/edit/{id}" method="post">
                            @csrf
                            <button>
                                Edit
                            </button>
                        </form>

                        <form action="/delete" method="post">
                            @csrf
                            <button>
                                Delete
                            </button>
                        </form>
                    </div>

                </div>
            </div>
        @endforeach
        <div class="mx-auto pb-10 w-4/5">
            {{$projects->links()}}
        </div>
    </div>
@endsection
