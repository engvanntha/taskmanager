<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
          integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <script src="//unpkg.com/alpinejs" defer></script>
    @vite('resources/css/app.css')
    <title>Task Manager</title>
</head>
<body>
<h1 class="text-9xl mt-5">
    {{--    Navbar      --}}
    <nav class="flex justify-between items-center mb-4">
        @auth
            <a href="{{route('index')}}">
                <img class="w-20" src="{{ asset('images/tm_logo.png') }}" alt="logo" class="logo"/>
            </a>
            <ul class="flex space-x-6 mr-6 text-lg">

                <li>
                    <span>Welcome</span>
                    <div class="inline text-xl font-bold">
                        {{ auth()->user()->name }}
                    </div>

                </li>
                <li>
                    <a href="components.settings" class="hover:text-appColor"
                    ><i class="fa-solid fa-user-gear"></i> Settings</a
                    >
                </li>
                <li>
                    <form action="/logout" method="post">
                        @csrf
                        <button type="submit" class="hover:text-appColor">
                            <i class="fa-solid fa-door-closed"></i> Logout</a>
                        </button>
                    </form>
                </li>
            </ul>
        @endauth
    </nav>
    <x-flash-message/>
</h1>

@yield('content')

</body>

</html>
