@extends('index')
@section('content')
    <div class="mx-4">
        <div class="bg-gray-50 border border-gray-200 p-10 rounded max-w-lg mx-auto mt-24">
            <header class="text-center">
                <h2 class="text-2xl font-bold uppercase mb-1">
                    LOGIN
                </h2>
                <p class="mb-4">Log in to save your works</p>
            </header>

            <form action="/login" method="post">
                @csrf
                <div class="mb-6">
                    <label for="email" class="inline-block text-lg mb-2"
                    >Email</label
                    >
                    <input
                        type="email"
                        class="border border-gray-200 rounded p-2 w-full"
                        name="email"
                        value="{{ old('email') }}"
                    />
                    @error('email')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>

                <div class="mb-6">
                    <label
                        for="password"
                        class="inline-block text-lg mb-2"
                    >
                        Password
                    </label>
                    <input
                        type="password"
                        class="border border-gray-200 rounded p-2 w-full"
                        name="password"
                        value="{{ old('password') }}"
                    />
                    @error('password')
                    <p class="text-red-500 text-xs mt-1">{{ $message }}</p>
                    @enderror
                </div>

                <div class="flex items-center mb-6">
                    <button
                        type="submit"
                        class="bg-appColor mr-3.5 text-white rounded py-2 px-4 hover:bg-black"
                    >
                        Log In
                    </button>
                    <div>
                        <span>Doesn't have account ? </span>
                        <a href="{{ route('users.register') }}" class="hover:text-appColor"
                        ><i class="fa-solid fa-user-plus"></i> Register</a
                        >
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection

