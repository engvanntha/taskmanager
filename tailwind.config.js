/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
    ],
  theme: {
    extend: {
        colors: {
            'appColor': '#EF4444',
            'title-color': '#F8F8F8'
        }
    },
  },
  plugins: [],
}
