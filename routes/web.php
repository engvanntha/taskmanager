<?php

use App\Http\Controllers\BoardController;
use App\Http\Controllers\CardController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Show Registration Page
Route::get('/register', [UserController::class, 'register'])->name('users.register');
// Do Registration
Route::post('/register', [UserController::class, 'doRegister']);
// Login User Out
Route::post('/logout', [UserController::class, 'logout']);
// Show Login Page
Route::get('/login', [UserController::class, 'login'])->name('users.login');
// Do Login
Route::post('/login', [UserController::class, 'doLogin']);

// Home Page shows all user's projects
Route::get('/', [ProjectController::class, 'projectListings'])->name('index');
// Create New Project
Route::post('/create-project', [ProjectController::class, 'createProject']);
//// Show Create Project/Board Form
//Route::get('/create-board', [BoardController::class, 'board']);
//Route::get('/create-board/{id}', [BoardController::class, 'board']);
//// Create board
//Route::post('/create-board', [BoardController::class, 'createBoard']);
//// Delete board
//Route::delete('/boards/{board}', [BoardController::class, 'deleteBoard']);
//
//// Create card
//Route::post('/add-card', [CardController::class, 'createCard']);
// Show settings
Route::get('/settings', [ProjectController::class, 'showSetting']);

//// Edit user info
//Route::put('/edit-info/{auth()->user()->id}}', [UserController::class, 'editUserInfo']);
